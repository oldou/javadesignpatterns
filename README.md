# Java23种设计模式源码以及博客教程

## 设计模式类型

将设计者的思维融入大家的学习和工作中，更高层次的思考！
这里有我整理好的Java23种设计模式的源码以及博客教程，博客教程中介绍了Java23种设计的模式的各种实现方式以及应用场景，非常适用于学习以及提高我们的设计思维，如果对大家有所帮助，请记得star一下给予作者一定的精神支持，你的star是我写出更好的博客的动力，谢谢大家。
持续更新中............

• **创建型模式**：
– 单例模式、工厂模式、抽象工厂模式、建造者模式、原型模式。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200914153700874.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI0NjIxNQ==,size_12,color_FFFFFF,t_70#pic_center)
• **结构型模式**：
– 适配器模式、桥接模式、装饰模式、组合模式、外观模式、享元模式、代理模式。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200914153422640.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI0NjIxNQ==,size_12,color_FFFFFF,t_70#pic_center)


• **行为型模式**：
– 模版方法模式、命令模式、迭代器模式、观察者模式、中介者模式、备忘录模式、解释器模式、状态模式、策略模式、职责链模式、访问者模式。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200914153917380.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzI0NjIxNQ==,size_12,color_FFFFFF,t_70#pic_center)

## 博客教程

### 创建型模式

1、[单例模式的五种实现方式（懒汉、饿汉、双重检查锁、静态内部类、枚举），反射破解单例、不能破解枚举单例模式详解](https://blog.csdn.net/weixin_43246215/article/details/107566719)

2、[工厂模式的实现（普通工厂、工厂方法）以及抽象工厂模式的实现](https://blog.csdn.net/weixin_43246215/article/details/107925455)

3、[建造者模式的实现](https://blog.csdn.net/weixin_43246215/article/details/108605226)

4、[原型模式的实现](https://blog.csdn.net/weixin_43246215/article/details/108590854)

### 结构型模式

1、[适配器模式的实现](https://blog.csdn.net/weixin_43246215/article/details/108585895)

2、桥接模式

3、[装饰模式](https://blog.csdn.net/weixin_43246215/article/details/108577241)

4、组合模式

5、外观模式

6、享元模式

7、[代理模式：静态代理、JDK动态代理的实现](https://blog.csdn.net/weixin_43246215/article/details/108599790)

### 行为型模式

1、模版方法模式

2、命令模式

3、迭代器模式

4、观察者模式

5、中介者模式

6、备忘录模式

7、解释器模式

8、状态模式

9、策略模式

10、职责链模式

11、访问者模式。



























