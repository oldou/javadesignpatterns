package com.oldou.builder.demo02;

public class Test {
    public static void main(String[] args) {
        //服务员
        Worker worker = new Worker();

        Product product = worker.getProduct();
        System.out.println("默认的套餐："+product);

        /** 自己选择套餐
         * 链式编程：在原来的基础上，可以自由组合套餐，如果不组合也有默认的套餐
         */
        Product product1 = worker.buildA("拉面")
                                 .buildB("雪碧")
                                 .getProduct();

        System.out.println("自己组合的套餐："+product1);
    }
}
