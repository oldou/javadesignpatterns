package com.oldou.builder.demo03;

/**
 * 指导者类：Director
 * 负责指挥构建哪种电脑
 */
public class Director {

    public void makeComputer(ComputerBuilder builder){
        builder.setUsbCount();
        builder.setDisplay();
        builder.setKeyboard();
    }

}
