package com.oldou.builder.demo03;

public class Client {
    public static void main(String[] args) {
        //创建指挥者 首先生成一个director
        Director director = new Director(); //1
        //然后生成一个目标builder
        ComputerBuilder builder = new MacComputerBuilder("I7处理器", "三星123");//2

        //接着使用director组装builder
        director.makeComputer(builder);//3

        //组装完毕后使用builder创建产品实例
        Computer computer = builder.getComputer();//4
        System.out.println("苹果电脑"+computer.toString());

        ComputerBuilder lenovoBuilder = new LenovoComputerBuilder("I9处理器", "海力士333");
        director.makeComputer(lenovoBuilder);
        Computer lenovoComputer = lenovoBuilder.getComputer();
        System.out.println("联想电脑："+lenovoComputer.toString());
    }
}
