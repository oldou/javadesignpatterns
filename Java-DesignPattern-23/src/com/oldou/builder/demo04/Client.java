package com.oldou.builder.demo04;

/**
 * 客户端
 */
public class Client {
    public static void main(String[] args) {
        Computer computer = new Computer.Builder("因特尔","三星")
                .setDisplay("三星24寸显示屏")
                .setKeyboard("小键盘")
                .setUsbCount(2)
                .build();
    }
}
