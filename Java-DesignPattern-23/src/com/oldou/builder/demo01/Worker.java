package com.oldou.builder.demo01;

//具体的建造者：工人
public class Worker extends Builder {
    private Product product;

    public Worker() {
        product = new Product();//由工人来创建具体的产品
    }

    @Override
    void buildA() {
        product.setBuildA("打地基");
        System.out.println("打地基");
    }

    @Override
    void buildB() {
        product.setBuildB("钢筋工程");
        System.out.println("钢筋工程完工");
    }

    @Override
    void buildC() {
        product.setBuildC("铺电线");
        System.out.println("铺电线OK");
    }

    @Override
    void buildD() {
        product.setBuildD("粉刷");
        System.out.println("粉刷完成");
    }

    @Override
    Product getProduct() {
        return product;
    }
}
