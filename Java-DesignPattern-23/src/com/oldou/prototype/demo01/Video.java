package com.oldou.prototype.demo01;

import java.util.Date;

/** 原型模式(浅克隆)
 * 第一步：实现 Cloneable 接口
 * 第二步： 重写一个方法---clone()
 */
public class Video implements Cloneable { //视频的原型

    private String name;//视频的名字
    private Date createTime;//视频的发布日期

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Video() {
    }
    public Video(String name, Date createTime) {
        this.name = name;
        this.createTime = createTime;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    @Override
    public String toString() {
        return "Video{" +
                "name='" + name + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
