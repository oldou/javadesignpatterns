package com.oldou.prototype.demo01;

import java.util.Date;

/**原型模式(浅克隆)
 * 客户端：克隆别人的视频
 */
public class Client {

    public static void main(String[] args) throws CloneNotSupportedException {
        //首先创建一个原型对象
        Date date = new Date();
        Video v1 = new Video("Java入坑指南",date);
        Video v2 = (Video)v1.clone();
        System.out.println("v1的信息---->"+v1);
        System.out.println("v2的信息---->"+v2);

        System.out.println("-------------------------------");
        date.setTime(22322121);//随意修改一下时间，说明时间是改变了
        System.out.println("v1的信息---->"+v1);
        System.out.println("v2的信息---->"+v2);

    }


}
/**
 *
 通过克隆v1创建出一个v2对象
 System.out.println("v2的信息---->"+v2);
 System.out.println("v2的HashCode为："+v2.hashCode());

 System.out.println("v1的信息---->"+v1);
 System.out.println("v1的HashCode为："+v1.hashCode());
 *
 *
 **/