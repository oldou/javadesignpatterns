package com.oldou.abstractfactory;

public interface Tyre {
    void revolve();//转速
}

class LuxuryTyre implements Tyre{

    @Override
    public void revolve() {
        System.out.println("高端轮胎——————旋转磨损小。。");
    }
}

class LowTyre implements Tyre{

    @Override
    public void revolve() {
        System.out.println("低端轮胎——————旋转磨损大。。");
    }
}

