package com.oldou.abstractfactory;

public class Client {
    public static void main(String[] args) {
        CarFactory carFactory = new LuxuryCarFactory();
        Engine e = carFactory.createEngine();
        e.run();
        e.start();
    }
}
