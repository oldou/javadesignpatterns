package com.oldou.abstractfactory;

public interface Seat {
    void massage(); //按摩方法
}

class LuxurySeat implements Seat{
    @Override
    public void massage() {
        System.out.println("高端按摩椅--可以自动按摩。。");
    }
}

class LowSeat implements Seat{
    @Override
    public void massage() {
        System.out.println("低端按摩椅--不可以动按摩。。");
    }
}
