package com.oldou.abstractfactory;

public interface CarFactory {
    Engine createEngine();//创建发动机的方法
    Seat createSeat();//创建座椅的方法
    Tyre createTyre();//创建轮胎的方法
}
