package com.oldou.abstractfactory;

public interface Engine {
    void run(); //发动机的运行
    void start(); //发动机的启动
}


class LuxuryEngine implements Engine{
    @Override
    public void run() {
        System.out.println("转得快..");
    }

    @Override
    public void start() {
        System.out.println("启动快..可以自动启停");
    }
}

class LowEngine implements Engine{
    @Override
    public void run() {
        System.out.println("转得慢..");
    }

    @Override
    public void start() {
        System.out.println("启动慢..");
    }
}

