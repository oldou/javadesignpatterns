package com.oldou.singleton;

/**
 * 双重检测锁模式的懒汉式，DCL懒汉式
 */
public class Demo03 {
    private static volatile Demo03 INSTANCE;
    private Demo03(){}

    public static Demo03 getInstance(){
        //先判断对象是否已经实例化过，没有实例化才能进入加锁代码
        if(INSTANCE==null){
            //对类对象加锁
            synchronized (Demo03.class){
                if(INSTANCE==null){
                    INSTANCE = new Demo03();
                }
            }
        }
        return INSTANCE;
    }
}
