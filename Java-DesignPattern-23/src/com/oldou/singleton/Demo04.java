package com.oldou.singleton;
/**
 * 静态内部类实现单例模式
 *
 * 优点：
 * 1、线程安全，还是懒加载模式
 * 2、调用效率高
 * 3、实现了延时加载
 */
public class Demo04 {
    //在静态内部类中定义单例对象，因为第一次初始化这个类时并不会立即初始化静态内部类
    private static class InnerClass{
        private static final Demo04 INSTANCE = new Demo04();
    }
    //构造器私有
    private Demo04(){}

    //通过静态内部类获取实例
    public static Demo04 getInstance(){
        return InnerClass.INSTANCE; //
    }
}
