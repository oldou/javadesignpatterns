package com.oldou.singleton;
/**
 * 懒汉式单例模式
 */
public class Demo02 {
    //类初始化时，不初始化这个对象(实现了延时加载，真正用到的时候才去创建)
    private static Demo02 INSTANCE;

    //构造器私有，一旦构造器私有，别人就无法去new这个对象了，保证了内存中只有这么一个对象
    private Demo02(){}

    //方法同步，调用效率低
    public static synchronized Demo02 getInstance(){
        if(INSTANCE==null){
            INSTANCE = new Demo02();
        }
        return INSTANCE;
    }

}
