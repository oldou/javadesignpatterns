package com.oldou.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 测试使用反射破解单例模式
 */
public class Demo06 {

        //测试使用反射破坏单例（懒汉、饿汉）
        public static void main(String[] args) throws Exception{
            Demo01 instance = Demo01.getInstance();
            //获取通过反射私有构造器（这里null表示获取无参）
            Constructor<Demo01> declaredConstructor = Demo01.class.getDeclaredConstructor(null);
            //破坏私有构造器
            declaredConstructor.setAccessible(true);
            //通过反射创建对象
            Demo01 instance2 = declaredConstructor.newInstance();
            //通过输出可以明显的看出创建了两个对象，单例模式被破解了
            System.out.println(instance);
            System.out.println(instance2);
        }
}

//测试通过反射是否能够破解枚举方式的单例模式
class Test{
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Demo05 instance1 = Demo05.INSTANCE;
        //使用反射获取无参构造器
        Constructor<Demo05> declaredConstructor = Demo05.class.getDeclaredConstructor(String.class,int.class);
        //将构造器的私有属性破除掉
        declaredConstructor.setAccessible(true);
        //获取对象
        Demo05 instance2 = declaredConstructor.newInstance();
        System.out.println(instance1);
        System.out.println(instance2);

    }
}
//结论：反射不能破解枚举方式的单例模式，详细的我会在博客中介绍。


