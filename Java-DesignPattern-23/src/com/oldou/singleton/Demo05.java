package com.oldou.singleton;
/**
 * 使用枚举方式实现单例模式
 • 优点：
 – 实现简单
 – 枚举本身就是单例模式。由JVM从根本上提供保障！避免通过反射和反序列化的漏洞！
 */
public enum Demo05 {
    /**
     *  定义一个枚举元素，它就代表了Singleton的一个实例
     */
    INSTANCE;
    public Demo05 getInstance(){
        return INSTANCE;
    }

}
