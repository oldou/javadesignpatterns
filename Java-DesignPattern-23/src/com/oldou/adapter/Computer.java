package com.oldou.adapter;

/**
 * 客户端类：想要上网，需要连接网线，但是只有USB接口，没有网线接口
 */
public class Computer {
    //电脑需要连接上转换器才能上网
    public void net(NetToUsb adapter){
        //上网的具体实现，转到一个转接头帮我们处理上网请求
        adapter.handleRequest();
    }

    public static void main(String[] args) {
        //想要上网，需要电脑、网线、适配器
        Computer computer = new Computer();//电脑
        Adaptee adaptee = new Adaptee();//网线
       // Adapter adapter = new Adapter();//转换器

        Adapter2 adapter2 = new Adapter2(adaptee);

        computer.net(adapter2);
    }


}
