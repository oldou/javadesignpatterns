package com.oldou.adapter;

/** 1.继承、类适配器
 * 真正的适配器
 * 需要提供USB转网线接口的功能，让电脑能够上网
 */
public class Adapter extends Adaptee implements NetToUsb {
    //以上使用继承的方式可以拥有网线上网的功能
    @Override
    public void handleRequest() {
        super.request();//拥有网线的上网功能，相当于转换连接了
    }
}
