package com.oldou.adapter;

/** 2.组合(对象适配器)---常用
 * 真正的适配器
 * 需要提供USB转网线接口的功能，让电脑能够上网
 */
public class Adapter2 implements NetToUsb {
    private Adaptee adaptee;

    public Adapter2(Adaptee adaptee) {
        this.adaptee = adaptee;
    }
    //以上使用继承的方式可以拥有网线上网的功能
    @Override
    public void handleRequest() {
        adaptee.request();//拥有网线的上网功能，相当于转换连接了
    }
}
