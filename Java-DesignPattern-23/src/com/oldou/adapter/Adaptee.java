package com.oldou.adapter;
/**
 * 要被适配的类  这里指例子中的网线
 */
public class Adaptee {
    public void request(){
        System.out.println("连接网线上网！");
    }
}
