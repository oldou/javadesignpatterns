package com.oldou.decorator;

/**
 * 抽象构建
 */
public interface ICar {
    void move();//跑
}

/** ConcreteComponent
 * 具体构建角色(真实对象)
 */
class Car implements ICar{
    @Override
    public void move() {
        System.out.println("陆地上跑.....");
    }
}

/**
 * 装饰器角色
 */
class SuperCar implements ICar{
    protected ICar car; //需要持有真实对象的引用
    public SuperCar(ICar car) {
        super();
        this.car = car;
    }
    @Override
    public void move() {
        car.move();
    }
}
/**
 * 具体的装饰器1
 */
class FlyCar extends SuperCar{

    public FlyCar(ICar car) {
        super(car);
    }
    //增加新的功能
    public void fly(){
        System.out.println("可以天上飞了");
    }

    @Override
    public void move() {
        super.move();//调用原有的
        fly();//增加新的功能
    }
}

/**
 * 具体的装饰器 2
 */
class WaterCar extends SuperCar{
    public WaterCar(ICar car) {
        super(car);
    }
    //新的功能
    public void swim(){
        System.out.println("可以水里游了。。。");
    }

    @Override
    public void move() {
        super.move();
        swim();//增加上水里游的功能
    }
}




