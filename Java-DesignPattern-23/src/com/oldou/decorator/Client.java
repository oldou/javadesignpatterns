package com.oldou.decorator;

import java.io.*;

/**
 * 设计模式之装饰者模式的测试
 */
public class Client {

    public static void main(String[] args) {

        Car car = new Car();
        car.move(); //真实角色只有在陆地上跑的功能

        System.out.println("增加新功能--");
        FlyCar flyCar = new FlyCar(car);//需要加入真实对象对其增加新的功能
        flyCar.move();
        System.out.println("**********************************");
        System.out.println("增加新的功能--");
        WaterCar waterCar = new WaterCar(car);
        waterCar.move();

        System.out.println("*******************两种功能都给安排上****************");
        WaterCar waterCar1 = new WaterCar(new FlyCar(new Car()));
        waterCar1.move();

       //Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(""))));
    }
    /**
     * 以上的测试代码和我们之前所学习的I/O留非常相似
     * 例如：Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(""))));
     * 这里的FileInputStream就相当于真实的对象，相当于我们这里的Car
     * InputStreamReader和 BufferedReader就相当于装饰器，我们这里的WaterCar和FlyCar
     */

}
