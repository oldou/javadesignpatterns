package com.oldou.factory.factorymethod;

public interface Car {
    void run();
}
