package com.oldou.factory.factorymethod;

public interface CarFactory {
    Car createCar();
}
