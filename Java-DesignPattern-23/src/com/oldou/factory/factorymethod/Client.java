package com.oldou.factory.factorymethod;

public abstract class Client implements CarFactory {

    public static void main(String[] args) {
        Car c1 = new BydFactory().createCar();
        Car c2 = new AudiFactory().createCar();

        c1.run();
        c2.run();
    }

}
