package com.oldou.factory.simplefactory;

/**
 * 简单工厂模式
 * 缺点：违反了开闭原则
 */
public abstract class CarFactory extends Audi { //创建者

    public static Car createCar(String type){
        if("奥迪".equals(type)){
            return new Audi();
        }else if("比亚迪".equals(type)){
            return new Byd();
        }else{
            return null;
        }
    }

}
