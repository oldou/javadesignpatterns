package com.oldou.factory.simplefactory;

/**
 * 简单工厂模式02
 *
 */
public  class CarFactory02 { //创建者

    public static Car createAudi(){
        return new Audi();
    }

    public static Car createByd(){
        return new Byd();
    }
}
