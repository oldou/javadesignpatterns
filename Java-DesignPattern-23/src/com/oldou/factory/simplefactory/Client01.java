package com.oldou.factory.simplefactory;

/**
 * 不使用工厂模式
 */
public abstract class Client01 extends CarFactory implements Car { //调用者
    public static void main(String[] args) {
        Car c1 = new Audi();
        Car c2 = new Byd();

        c1.run();
        c2.run();

    }
}
