package com.oldou.factory.simplefactory;

public interface Car {
    void run();
}
