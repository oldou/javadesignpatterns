package com.oldou.proxy.JdkProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 这个类会自动的生成代理类
 *
 */
public class ProxyInvocationHandler implements InvocationHandler {
    //被代理的接口
    private Rent rent;
    public void setRent(Rent rent) {
        this.rent = rent;
    }

    //生成得到代理类
    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(),
                rent.getClass().getInterfaces(),this);
    }


    //处理代理实例并返回结果
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //动态代理的本质就是使用反射机制实现的
        seeHouse(); //扩展操作
        Object result = method.invoke(rent, args);
        writeHeTong();//扩展操作
        return result;
    }

    //扩展操作
    public void seeHouse(){
        System.out.println("中介带你看房子");
    }
    public void writeHeTong(){
        System.out.println("签署合同");
    }
}
