package com.oldou.proxy.JdkProxy.demo;

import com.oldou.proxy.staticProxy.demo.UserService;
import com.oldou.proxy.staticProxy.demo.UserServiceImpl;

public class Client {

    public static void main(String[] args) {
        //真实角色
        UserServiceImpl userService = new UserServiceImpl();

        //代理角色  不存在
        ProxyInvocationHandler pih = new ProxyInvocationHandler();

        //设置要代理的对象
        pih.setTarget(userService);

        //动态的为其生成代理对象
        UserService proxy = (UserService)pih.getProxy();

        proxy.add();
    }

}
