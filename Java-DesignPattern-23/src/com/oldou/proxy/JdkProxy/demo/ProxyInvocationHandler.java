package com.oldou.proxy.JdkProxy.demo;

import com.oldou.proxy.JdkProxy.Rent;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 这个类会自动的生成代理类
 *
 */
public class ProxyInvocationHandler implements InvocationHandler {
    //被代理的目标接口
    private Object target;
    public void setTarget(Object target) {
        this.target = target;
    }

    //生成得到代理类
    public Object getProxy(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(),
                target.getClass().getInterfaces(),this);
    }


    //处理代理实例并返回结果
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //动态代理的本质就是使用反射机制实现的
        log(method.getName());
        Object result = method.invoke(target, args);

        return result;
    }

    //扩展操作--日志
    public void log(String msg){
        System.out.println("[Debug]--->执行了"+msg+"方法");
    }

}
