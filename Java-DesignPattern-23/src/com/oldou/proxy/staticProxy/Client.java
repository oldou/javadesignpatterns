package com.oldou.proxy.staticProxy;

/** 静态代理模式
 * 真实的人：需要租房的人--客户
 */
public class Client {

    public static void main(String[] args) {
        //房东要出租房子
        FangD fd = new FangD();

        //代理，中介要帮房东出租房子，并且中介还可以做一些附属操作
        Proxy proxy = new Proxy(fd);

        //不用找房东，直接找中介租房
        proxy.rent();

        /** 优点：
         * 房东只需要找到一个代理出租房子，其他操作就交给代理对象了
         * 这样可以使真实角色的操作更加纯粹
         * 公共业务交给代理角色，实现业务分工
         * 公共业务发生扩展的时候，方便集中管理
         */
        /**缺点：
         *  一个真实角色就会产生一个代理角色，代码量会翻倍，开发效率会变低
         */

    }

}
