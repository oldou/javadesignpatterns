package com.oldou.proxy.staticProxy;

/**
 * 代理角色：中介
 * 需要代理房东去租房，因此也要实现租房的方法
 */
public class Proxy implements Rent {
    private FangD fd;
    //提供一个房东的有参构造
    public Proxy(FangD fd) {
        this.fd = fd;
    }
    public Proxy(){}

    @Override
    public void rent() {
        seeHouse(); //中介这里扩展的操作  看房
        fd.rent();//调用房东的租房方法
        hetong();//签合同
        fare();//收取中介费
    }

    //看房
    public void seeHouse(){
        System.out.println("中介带你看房..");
    }
    //收中介费
    public void fare(){
        System.out.println("中介收你中介费100元..");
    }
    //签合同
    public void hetong(){
        System.out.println("签合同...");
    }
}
