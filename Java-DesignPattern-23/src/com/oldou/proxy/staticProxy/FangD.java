package com.oldou.proxy.staticProxy;

/**
 * 真实的角色：房东
 * 需要将房子租出去
 */
public class FangD implements Rent {
    @Override
    public void rent() {
        System.out.println("出租XXX房，两室一厅....");
    }
}
