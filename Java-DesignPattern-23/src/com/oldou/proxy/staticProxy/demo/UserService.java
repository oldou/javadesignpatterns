package com.oldou.proxy.staticProxy.demo;

public interface UserService {
    void add();
    void delete();
    void update();
    void query();
}
