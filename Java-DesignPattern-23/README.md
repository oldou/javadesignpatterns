本项目为Java的23种设计模式的源码
以下介绍各种包名所代表的设计模式：
# 创建型模式
## singleton：单例模式
 - Demo01：饿汉式
 - Demo02：懒汉式
 - Demo03：双重检查锁方式
 - Demo04：静态内部类方式
 - Demo05：枚举
 - Demo06：反射破解单例模式
## factory：工厂模式
 - simplefactory：简单工厂模式
 - fatcorymethod：工厂方法模式

## abstractfactory：抽象工厂模式

## builder：建造者模式
  - demo01: 建房子案例(有点难理解)
  - demo02：肯德基点餐案例(有点难理解)
  - demo03: 电脑的案例 （推荐，好理解）
  - <font color="red">demo04：使用静态内部类的方式实现原型模式</font>
## prototype：原型模式
 - demo01：浅克隆的代码测试
 - demo02：深克隆的代码测试



# 结构型模式
## adapter：适配器模式

## proxy：代理模式
 - staticProxy：静态代理 、demo:静态代理的业务实现  
  
 - JdkProxy：JDK动态代理 、demo:动态代理的业务实现


## decorator：装饰模式

# 行为型模式



持续更新中........












