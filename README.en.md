# Java23种设计模式源码以及博客教程

#### Description
该仓库为学习Java的23种设计模式时用于存放所写的笔记以及源码，文章中详细的介绍了关于每种设计模式的设计思路介绍以及扩展知识，希望对大家有所帮助，同时也希望大家能够star支持一下，有了大家的支持才能够写出更好的教程。

#### Software Architecture
Software architecture description

#### Installation

1.  单例模式的五种实现方式
2.  工厂模式和抽象工厂模式
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
